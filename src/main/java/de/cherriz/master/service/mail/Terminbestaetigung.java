package de.cherriz.master.service.mail;

/**
 * Enthaelt alle notwendigen Informationen fuer das Verarbeiten einer Terminbestaetigung.
 *
 * @author Frederik Kirsch
 */
public class Terminbestaetigung {

    private Long vertreterID = null;

    private Long kontaktID = null;

    private Long terminID = null;

    /**
     * @return Die ID des Vertreters.
     */
    public Long getVertreterID() {
        return vertreterID;
    }

    /**
     * Setzt den Vertreter.
     *
     * @param vertreterID Der Vertreter.
     */
    public void setVertreterID(Long vertreterID) {
        this.vertreterID = vertreterID;
    }

    /**
     * @return Der Kontakt.
     */
    public Long getKontaktID() {
        return kontaktID;
    }

    /**
     * Setzt den Kontakt.
     *
     * @param kontaktID Der Kontakt.
     */
    public void setKontaktID(Long kontaktID) {
        this.kontaktID = kontaktID;
    }

    /**
     * @return Der Termin.
     */
    public Long getTerminID() {
        return terminID;
    }

    /**
     *
     * @param terminID
     */
    public void setTerminID(Long terminID) {
        this.terminID = terminID;
    }

}