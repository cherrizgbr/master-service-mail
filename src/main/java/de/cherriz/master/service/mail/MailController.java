package de.cherriz.master.service.mail;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Logger;

/**
 * Controller fuer den REST Service fuer Mails.
 *
 * @author Frederik Kirsch
 */
@RestController
@RequestMapping(value = "/mail")
public class MailController {

    private final static Logger LOGGER = Logger.getLogger(MailController.class.getName());

    /**
     * Methode dient zur Pruefung der Verfuegbarkeit des Services.
     *
     * @return den Status des Services.
     */
    @RequestMapping(value = "/systemcheck", method = RequestMethod.GET)
    public String hello() {
        String result = "SYSTEMCHECK:";
        return result;
    }

    /**
     * Loest eine Terminbestaetigung aus.
     *
     * @param terminbestaetigung Die Termnbestaetigung.
     */
    @RequestMapping(value = "/terminbestaetigung", method = RequestMethod.PUT)
    public void sendTerminbestaetigung(@RequestBody @Valid Terminbestaetigung terminbestaetigung) {
        String info = "Terminbestätigung versenden:\n";
        info += "\tKontaktID:\t" + terminbestaetigung.getKontaktID() + "\n";
        info += "\tVertreterID:\t" + terminbestaetigung.getVertreterID() + "\n";
        info += "\tTerminID:\t" + terminbestaetigung.getTerminID();
        LOGGER.info(info);
    }

    /**
     * Versendet Informationsmaterial.
     *
     * @param id Der Empfaenger.
     */
    @RequestMapping(value = "/infomaterial/{id}", method = RequestMethod.PUT)
    public void sendInfoMaterial(@PathVariable Long id) {
        String info = "Infomaterial versenden:\n";
        info += "\tKontaktID:\t" + id;
        LOGGER.info(info);
    }

}